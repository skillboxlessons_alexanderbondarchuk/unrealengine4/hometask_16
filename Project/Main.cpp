#include <iostream>
#include <ctime>

void main() {
	const auto N{ 10 };
	int array[N][N];

	// ������ ������ � ����� ������� ��� �������� � �������
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			array[i][j] = i + j;
			std::cout << array[i][j] << " ";
		}
		std::cout << std::endl;
	}

	time_t rawTime;
	struct tm timeInfo;

	time(&rawTime);// ���������� UNIX ����
	localtime_s(&timeInfo, &rawTime); // ��������������� � ��������� �����

	// ������� �� ������� ����������� ������ ��� ������ �� N
	auto reminder = timeInfo.tm_mday % N;
	auto sum{ 0 };

	// ��������� ����� ��������� ������ �������
	for (int i = 0; i < N; i++) {
		sum += array[reminder][i];
	}

	std::cout << "Sum: " << sum;
}